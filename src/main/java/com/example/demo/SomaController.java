package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
class SomaController {

    @PostMapping("/soma/{cpf}")
    ResultadoDecorator home(
            @RequestBody String requestBody,
            @PathVariable String cpf,
            @RequestAttribute(value = "status", required = false) String status
    ) {
        Resultado resultado = new Resultado();
        for (Transacao t: createTransactions()) {

            // Resultado
            resultado.setValor(resultado.getValor().add(t.getValor()));

            // Macro
            ResultadoMacro resultadoMacro = resultado.getMacros().get(t.getMacro());
            if (resultadoMacro == null) {
                resultadoMacro = new ResultadoMacro();
                resultadoMacro.setMacro(t.getMacro());
                resultado.getMacros().put(t.getMacro(), resultadoMacro);
            }
            resultadoMacro.setValor(resultadoMacro.getValor().add(t.getValor()));

            // Micro
            ResultadoMicro resultadoMicro = resultadoMacro.getMicros().get(t.getMicro());
            if (resultadoMicro == null) {
                resultadoMicro = new ResultadoMicro();
                resultadoMicro.setMicro(t.getMicro());
                resultadoMacro.getMicros().put(t.getMicro(), resultadoMicro);
            }
            resultadoMicro.setValor(resultadoMicro.getValor().add(t.getValor()));

        }
        return new ResultadoDecorator(resultado);
    }

    private List<Transacao> createTransactions() {
        List<Transacao> list = new ArrayList<Transacao>();
        for (int mes = 1; mes < 5; mes++) {
            for (int macro = 1; macro < 4; macro++) {
                for (int micro = 1; micro < 4; micro++) {
                    list.add(new Transacao(
                            BigDecimal.ONE,
                            String.valueOf(mes),
                            "micro_" + micro,
                            "macro_" + macro
                    ));
                }
            }
        }
        return list;
    }
}

