package com.example.demo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ResultadoMicro {
    private String micro;
    private BigDecimal valor = BigDecimal.ZERO;
}
