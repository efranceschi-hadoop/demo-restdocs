package com.example.demo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class ResultadoMacro {

    private String macro;
    private BigDecimal valor = BigDecimal.ZERO;
    private Map<String, ResultadoMicro> micros = new HashMap();

}
