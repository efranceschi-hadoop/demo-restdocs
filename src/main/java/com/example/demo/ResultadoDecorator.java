package com.example.demo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

public final class ResultadoDecorator {
    private final Resultado resultado;

    public ResultadoDecorator(Resultado resultado) {
        this.resultado = resultado;
    }

    public BigDecimal getValor() {
        return resultado.getValor();
    }

    public Collection<ResultadoMacroDecorator> getMacros() {
        return resultado.getMacros().values().stream()
                .map(m -> new ResultadoMacroDecorator(m))
                .collect(Collectors.toList());
    }
}
