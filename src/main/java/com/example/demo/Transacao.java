package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class Transacao {

    private BigDecimal valor;
    private String mes;
    private String micro;
    private String macro;

}
