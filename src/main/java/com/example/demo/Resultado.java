package com.example.demo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class Resultado {
    private Map<String, ResultadoMacro> macros = new HashMap<>();
    private BigDecimal valor = BigDecimal.ZERO;
}
