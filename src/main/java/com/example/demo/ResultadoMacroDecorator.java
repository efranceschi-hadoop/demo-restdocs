package com.example.demo;

import java.math.BigDecimal;
import java.util.Collection;

public class ResultadoMacroDecorator {
    final ResultadoMacro resultadoMacro;

    public ResultadoMacroDecorator(ResultadoMacro resultadoMacro) {
        this.resultadoMacro = resultadoMacro;
    }

    public String getMacro() {
        return resultadoMacro.getMacro();
    }

    public BigDecimal getValor() {
        return resultadoMacro.getValor();
    }

    public Collection<ResultadoMicro> getMicros() {
        return resultadoMacro.getMicros().values();
    }
}
