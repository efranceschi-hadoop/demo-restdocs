package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(SomaController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class SomaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    public static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Test
    public void somaTest() throws Exception {
        this.mockMvc
                .perform(
                        post("/soma/{cpf}?status={status}", "12345678901", "123")
                                .contentType(APPLICATION_JSON_UTF8)
                                .content("{ \"user\": 1 }")
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                // testes
                .andDo(document("soma",
                        requestParameters(
                                parameterWithName("status").description("Status")
                        ),
                        pathParameters(
                                parameterWithName("cpf").description("CPF do cliente")
                        ),
                        requestFields(
                                fieldWithPath("user").description("ID do usuário")
                        ),
                        responseFields(
                                fieldWithPath("valor").description("Soma de todas as categorias"),
                                fieldWithPath("macros[]").description("Lista de macro categorias"),
                                fieldWithPath("macros[].macro").description("Nome da macro categoria"),
                                fieldWithPath("macros[].valor").description("Valor total da Macro"),
                                fieldWithPath("macros[].micros[]").description("Lista de micro Categorias"),
                                fieldWithPath("macros[].micros[].micro").description("Micro Categoria"),
                                fieldWithPath("macros[].micros[].valor").description("Valor desta micro categoria")
                        )
                ));
    }

}
